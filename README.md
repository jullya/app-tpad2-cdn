Objetivos:
- Verificar se ocorreu alguma mudança em relação às expectativas iniciais dos alunos devido à vivência acadêmica;
- Verificar se a grade curricular presente está atualizada de acordo com as inovações tecnológicas e novos dados em relação ao seu curso;
- Compreender os tipos de empecilhos presentes na vida dos alunos que desestemulam-no a dar continuidado à sua graduação.

## Requisitos para o projeto

Instale todos os pacotes necesários
'''
python -m venv venv
source venv/Scripts/activate
pip install --upgrade pip

pip install -r requirements.txt #windows
pip3 install -r requirements.txt #mac
'''

## Atualizar bibliotecas
pip install $(pip list --outdated | awk '{ print $1 }') --upgrade.